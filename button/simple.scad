



module button_blank ( width, lip ) {
    union() {
        rotate_extrude(convexity = 10, $fn = 100)
        translate([width, 0, 0])
        circle(r = lip, $fn = 100);

        translate([0, 0, (0-lip)])
        cylinder(h=lip, r1=width, r2=width, center=false);
    }
}

module hole_placer (quantity, radius, dist, height) {
    for (step =[1:quantity]){
        translate([(dist * sin(step*(360/quantity))),(dist*cos(step*(360/quantity))),0])
        cylinder(h=5, r1=radius, r2=radius, $fn=18, center=true);        
    }
}

module hole_puncher(width, lip, holes, holesize, spread) {
    difference() {
        button_blank(width,lip);
        hole_placer(holes,holesize,spread,lip);
    }
}

hole_puncher(12,1.2,6,1.2,4.9);